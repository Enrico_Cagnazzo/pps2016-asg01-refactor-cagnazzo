package utils;

/**
 * @author Roberto Casadei
 */

public class Res {
    public static final String IMG_BASE = "/resources/imgs/";
    public static final String AUDIO_BASE = "/resources/audio/";
    public static final String IMG_EXT = ".png";

    public static final String IMAGE_PREFIX_STATUS_NORMAL = "";
    public static final String IMAGE_PREFIX_STATUS_ACTIVE = "A";
    public static final String IMAGE_PREFIX_STATUS_DEAD = "E";
    public static final String IMAGE_PREFIX_STATUS_SUPER = "S";

    public static final String IMAGE_PREFIX_DIRECTION_SX = "G";
    public static final String IMAGE_PREFIX_DIRECTION_DX = "D";

    public static final String IMAGE_PREFIX_CHARACTER_MUSHROOM = "mushroom";
    public static final String IMAGE_PREFIX_CHARACTER_TURTLE = "turtle";
    public static final String IMAGE_PREFIX_CHARACTER_MARIO = "mario";

    private static final String IMAGE_PREFIX_OBJECT_BLOCK = "block";
    private static final String IMAGE_PREFIX_OBJECT_MONEY1 = "money1";
    private static final String IMAGE_PREFIX_OBJECT_MONEY2 = "money2";
    private static final String IMAGE_PREFIX_OBJECT_TUNNEL = "pipe";

    public static final String IMG_MARIO_DEFAULT = IMG_BASE + IMAGE_PREFIX_CHARACTER_MARIO + IMAGE_PREFIX_DIRECTION_DX + IMG_EXT;
    public static final String IMG_MARIO_SUPER_SX = IMG_BASE + IMAGE_PREFIX_CHARACTER_MARIO + IMAGE_PREFIX_STATUS_SUPER + IMAGE_PREFIX_DIRECTION_SX + IMG_EXT;
    public static final String IMG_MARIO_SUPER_DX = IMG_BASE + IMAGE_PREFIX_CHARACTER_MARIO + IMAGE_PREFIX_STATUS_SUPER + IMAGE_PREFIX_DIRECTION_DX + IMG_EXT;
    public static final String IMG_MARIO_ACTIVE_SX = IMG_BASE + IMAGE_PREFIX_CHARACTER_MARIO + IMAGE_PREFIX_STATUS_ACTIVE + IMAGE_PREFIX_DIRECTION_DX + IMG_EXT;
    public static final String IMG_MARIO_ACTIVE_DX = IMG_BASE + IMAGE_PREFIX_CHARACTER_MARIO + IMAGE_PREFIX_STATUS_ACTIVE + IMAGE_PREFIX_DIRECTION_DX + IMG_EXT;

    public static final String IMG_BLOCK = IMG_BASE + IMAGE_PREFIX_OBJECT_BLOCK + IMG_EXT;

    public static final String IMG_PIECE1 = IMG_BASE + IMAGE_PREFIX_OBJECT_MONEY1 + IMG_EXT;
    public static final String IMG_PIECE2 = IMG_BASE + IMAGE_PREFIX_OBJECT_MONEY2 + IMG_EXT;
    public static final String IMG_TUNNEL = IMG_BASE + IMAGE_PREFIX_OBJECT_TUNNEL + IMG_EXT;

    public static final String IMG_BACKGROUND = IMG_BASE + "background" + IMG_EXT;
    public static final String IMG_CASTLE = IMG_BASE + "startCastle" + IMG_EXT;
    public static final String START_ICON = IMG_BASE + "start" + IMG_EXT;
    public static final String IMG_CASTLE_FINAL = IMG_BASE + "finalCastle" + IMG_EXT;
    public static final String IMG_FLAG = IMG_BASE + "flag" + IMG_EXT;

    public static final String AUDIO_MONEY = AUDIO_BASE + "money.wav";
    public static final String AUDIO_SOUNDTRACK=AUDIO_BASE+"soundtrack.wav";
    public static final String AUDIO_JUMP=AUDIO_BASE+"jump.wav";
}
