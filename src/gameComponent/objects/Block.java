package gameComponent.objects;

import utils.Res;
import utils.Utils;

public class Block extends GameObject {

    private static final int WIDTH = 30;
    private static final int HEIGHT = 30;

    public Block(int x, int y) {
        super(x, y, WIDTH, HEIGHT);
        super.setImageObject(Utils.getImage(Res.IMG_BLOCK));
    }

}
