package gameComponent.objects;

import utils.Res;
import utils.Utils;

import java.awt.Image;

public class Money extends GameObject implements Runnable {

    private static final int WIDTH = 30;
    private static final int HEIGHT = 30;
    private static final int PAUSE = 10;
    private static final int FLIP_FREQUENCY = 100;
    private int counter=0;
    private boolean showFirstFrame=true;

    public Money(int x, int y) {
        super(x, y, WIDTH, HEIGHT);
        super.setImageObject(Utils.getImage(Res.IMG_PIECE1));
    }

    public String imageOnMovement() {
        if (++this.counter % FLIP_FREQUENCY == 0)
            showFirstFrame=!showFirstFrame;
        return showFirstFrame ? Res.IMG_PIECE1 : Res.IMG_PIECE2;
    }

    @Override
    public void run() {
        while (true) {
            this.imageOnMovement();
            try {
                Thread.sleep(PAUSE);
            } catch (InterruptedException e) {
            }
        }
    }

}
