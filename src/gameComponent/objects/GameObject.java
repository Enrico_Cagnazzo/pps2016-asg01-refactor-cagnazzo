package gameComponent.objects;

import java.awt.Image;

import game.Main;
import gameComponent.GameComponent;

public class GameObject implements GameComponent {

    private int width, height;
    private int x, y;


    private Image imageObject;

    public GameObject(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Image getImageObject() {
        return imageObject;
    }

    public void setImageObject(Image imageObject) {
        this.imageObject = imageObject;
    }

    public void move() {
        if (Main.scene.getXPos() >= 0) {
            this.x = this.x - Main.scene.getxSpeed();
        }
    }

}
