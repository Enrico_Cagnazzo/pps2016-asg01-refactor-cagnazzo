package gameComponent;

/**
 * Created by Enrico on 11/03/2017
 */
public interface GameComponent {

    int getWidth();
    int getHeight();
    int getX();
    int getY();

}
