package gameComponent.characters;

public class Mushroom extends BasicCharacter implements Runnable,Enemy {

    private static final int WIDTH = 27;
    private static final int HEIGHT = 30;

    public Mushroom(int x, int y) {
        super(x, y, WIDTH, HEIGHT);
        this.setToRight(true);
        this.setMoving(true);

        Thread mushroomThread = new Thread(this);
        mushroomThread.start();
    }

}
