package gameComponent.characters;

/**
 * Created by Enrico on 12/03/2017.
 */
public interface CharactersFactory {
    Mario    createMario(int x, int y);
    Mushroom createAMushroom(int x, int y);
    Turtle   createATurtle(int x,int y);
}
