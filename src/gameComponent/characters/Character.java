package gameComponent.characters;

import gameComponent.GameComponent;

public interface Character extends GameComponent {
    String walkImageURL(String name, int frequency);
    String deadImageURL(String name);

    boolean isNearby(GameComponent gameComponent);

    boolean hitAhead(GameComponent gameComponent);
    boolean hitBack (GameComponent gameComponent);
    boolean hitBelow(GameComponent gameComponent);
    boolean hitAbove(GameComponent gameComponent);

    void contact(GameComponent gameComponent);

    void move();
    boolean isAlive();

    void die();
}
