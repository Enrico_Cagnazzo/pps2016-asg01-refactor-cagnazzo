package gameComponent.characters;

import game.Audio;
import game.Main;
import gameComponent.GameComponent;
import gameComponent.objects.Money;
import utils.Res;

public class Mario extends BasicCharacter {

    private static final int FLOOR_OFFSET_Y_INITIAL = 293;
    private static final int WIDTH = 28;
    private static final int HEIGHT = 50;
    private static final int JUMPING_LIMIT = 42;
    private static final int FALL_VERTICAL_SPEED=1;
    private static final int JUMP_VERTICAL_SPEED=4;
    private static final int X_SPEED=1;

    private boolean jumping;
    private int jumpingExtent;

    public Mario(int x, int y) {
        super(x, y, WIDTH, HEIGHT);
        this.jumping = false;
        this.jumpingExtent = 0;
    }

    public boolean isJumping() {
        return jumping;
    }

    public void setJumping(boolean jumping) {
        this.jumping = jumping;
    }

    public int getXSpeed() {
        return (!this.isMoving())?0:this.isToRight()?X_SPEED:-X_SPEED;
    }

    public String doJump() {
        String imageName;

        this.jumpingExtent++;

        if (this.jumpingExtent < JUMPING_LIMIT) {
            if (this.getY() > Main.scene.getHeightLimit())
                this.setY(this.getY()- JUMP_VERTICAL_SPEED);
            else this.jumpingExtent = JUMPING_LIMIT;

            imageName = this.isToRight() ? Res.IMG_MARIO_SUPER_DX : Res.IMG_MARIO_SUPER_SX;
        } else if (this.getY() + this.getHeight() < Main.scene.getFloorOffsetY()) {
            this.setY(this.getY() + FALL_VERTICAL_SPEED);
            imageName = this.isToRight() ? Res.IMG_MARIO_SUPER_DX : Res.IMG_MARIO_SUPER_SX;
        } else {
            imageName = this.isToRight() ? Res.IMG_MARIO_ACTIVE_DX : Res.IMG_MARIO_ACTIVE_SX;
            this.jumping = false;
            this.jumpingExtent = 0;
        }

        return imageName;
    }

    private void letFall(){
        this.jumping=true;
        this.jumpingExtent=JUMPING_LIMIT;
    }

    public void contact(GameComponent object) {
        if (this.hitAhead(object) && this.isToRight() || this.hitBack(object) && !this.isToRight()) {
            Main.scene.setxSpeed(0);
            this.setMoving(false);
        }

        if (this.hitBelow(object) && this.jumping) {
            Main.scene.setFloorOffsetY(object.getY());
        } else if (!this.hitBelow(object)) {
            Main.scene.setFloorOffsetY(FLOOR_OFFSET_Y_INITIAL);
            if (this.hitAbove(object)) {
                Main.scene.setHeightLimit(object.getY() + object.getHeight()); // the new sky goes below the object
            } else if (!this.hitAbove(object) && !this.jumping) {
                Main.scene.setHeightLimit(0); // initial sky
            }
            if (!this.jumping) {
                letFall();
            }
        }
    }

    public boolean contactPiece(Money money) {
        return this.hitBack(money) || this.hitAbove(money) || this.hitAhead(money) || this.hitBelow(money);
    }

    public void move() {
        if (Main.scene.getXPos() >= 0) {
            this.setX(this.getX() - Main.scene.getxSpeed());
        }
    }

    public void contact(Enemy character) {
        if (this.hitAhead(character) || this.hitBack(character)) {
            if (character.isAlive()) {
                this.die();
            }
        } else if (this.hitBelow(character)) {
            character.die();
        }
    }

    public void die(){
        super.die();
        new Audio(Res.AUDIO_MONEY).playLoop();
    }


}
