package gameComponent.characters;

public class Turtle extends BasicCharacter implements Runnable,Enemy {

    private static final int WIDTH = 43;
    private static final int HEIGHT = 50;

    public Turtle(int X, int Y) {
        super(X, Y, WIDTH, HEIGHT);
        super.setToRight(true);
        super.setMoving(true);

        Thread turtleThread = new Thread(this);
        turtleThread.start();
    }

}
