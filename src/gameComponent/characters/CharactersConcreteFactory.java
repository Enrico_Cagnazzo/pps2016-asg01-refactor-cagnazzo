package gameComponent.characters;

/**
 * Created by Enrico on 12/03/2017
 */

public class CharactersConcreteFactory implements CharactersFactory{
    @Override
    public Mario createMario(int x, int y) {
        return new Mario(x,y);
    }

    @Override
    public Mushroom createAMushroom(int x, int y) {
        return new Mushroom(x,y);
    }

    @Override
    public Turtle createATurtle(int x, int y) {
        return new Turtle(x,y);
    }
}
