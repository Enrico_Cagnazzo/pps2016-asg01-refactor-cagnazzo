package gameComponent.characters;

import gameComponent.GameComponent;
import utils.Res;

public class BasicCharacter implements Runnable, Character {

    private static final int PROXIMITY_MARGIN = 10;
    private static final int OBJECT_OFFSET=5;
    private static final int SPEED_X=1;
    private final int PAUSE = 15;
    private int width, height;
    private int x, y;
    private boolean moving;
    private boolean toRight;
    private int counter;
    private boolean alive;
    private boolean showFirstFrame=true;

    protected BasicCharacter(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.height = height;
        this.width = width;
        this.counter = 0;
        this.moving = false;
        this.toRight = true;
        this.alive = true;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public boolean isAlive() {
        return alive;
    }

    public boolean isToRight() {
        return toRight;
    }

    public boolean isMoving() {
        return moving;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setMoving(boolean moving) {
        this.moving = moving;
    }

    public void setToRight(boolean toRight) {
        this.toRight = toRight;
    }

    public String imageURL(String name, String status){
        String string=Res.IMG_BASE + name+status;
        string+=this.toRight ? Res.IMAGE_PREFIX_DIRECTION_DX : Res.IMAGE_PREFIX_DIRECTION_SX;
        string+=Res.IMG_EXT;
        return string;
    }

    public String walkImageURL(String name, int frequency) {
        if (++this.counter%frequency==0){
            showFirstFrame=!showFirstFrame;
        }
        String status = !this.moving || !showFirstFrame ? Res.IMAGE_PREFIX_STATUS_ACTIVE : Res.IMAGE_PREFIX_STATUS_NORMAL;
        return imageURL(name,status);
    }

    public String deadImageURL(String name){
        return imageURL(name,Res.IMAGE_PREFIX_STATUS_DEAD);
    }

    public void move() {
        this.x+=isToRight() ? SPEED_X : -SPEED_X;
    }

    public void die(){
        this.moving=false;
        this.alive=false;
    }

    private boolean sameXPosition(GameComponent og){
        return this.x+this.width>=og.getX()+OBJECT_OFFSET && this.x<=og.getX()+og.getWidth()-OBJECT_OFFSET;
    }

    private boolean sameYPosition(GameComponent og){
        return this.y+this.height>og.getY() && this.y<og.getY()+og.getHeight();
    }

    public boolean hitAhead(GameComponent og) {
        return  this.x+this.width>=og.getX() && this.x+this.width<=og.getX()+OBJECT_OFFSET &&
                sameYPosition(og);
    }

    public boolean hitBack(GameComponent og) {
        return  this.x+this.width>=og.getX()+og.getWidth()-OBJECT_OFFSET && this.x<=og.getX()+og.getWidth() &&
                sameYPosition(og);
    }

    public boolean hitBelow(GameComponent og) {
        return  sameXPosition(og) &&
                this.y+this.height>=og.getY()  && this.y+this.height<=og.getY()+OBJECT_OFFSET;
    }

    public boolean hitAbove(GameComponent og) {
        return  sameXPosition(og) &&
                this.y>=og.getY()+og.getHeight() && this.y<=og.getY()+og.getHeight()+OBJECT_OFFSET;
        }

    public boolean isNearby(GameComponent component) {
        return  (this.x>component.getX()-PROXIMITY_MARGIN && this.x<component.getX()+component.getWidth()+PROXIMITY_MARGIN) ||
                (this.x+this.width>component.getX()-PROXIMITY_MARGIN &&
                        this.x+this.width<component.getX()+component.getWidth()+PROXIMITY_MARGIN);
    }

    public void contact(GameComponent component) {
        if (this.hitAhead(component) && this.isToRight()) {
            this.setToRight(false);
        } else if (this.hitBack(component) && !this.isToRight()) {
            this.setToRight(true);
        }
    }

    @Override
    public void run() {
        while (this.isAlive()) {
            this.move();
            try {
                Thread.sleep(PAUSE);
            } catch (InterruptedException e) {
            }
        }
    }
}
