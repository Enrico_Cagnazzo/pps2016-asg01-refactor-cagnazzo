package game;

import gameComponent.characters.CharactersConcreteFactory;
import gameComponent.characters.CharactersFactory;
import gameComponent.characters.Enemy;
import gameComponent.characters.Mario;
import gameComponent.objects.Block;

import java.awt.Graphics;
import java.awt.Image;
import java.util.ArrayList;

import javax.swing.JPanel;

import gameComponent.objects.GameObject;
import gameComponent.objects.Money;
import gameComponent.objects.Tunnel;
import utils.Res;
import utils.Utils;

@SuppressWarnings("serial")
public class Platform extends JPanel {

    private static final int MARIO_FREQUENCY = 25;
    private static final int MUSHROOM_FREQUENCY = 45;
    private static final int TURTLE_FREQUENCY = 45;
    private static final int MUSHROOM_DEAD_OFFSET_Y = 20;
    private static final int TURTLE_DEAD_OFFSET_Y = 30;
    private static final int FLAG_X_POS = 4650;
    private static final int CASTLE_X_POS = 4850;
    private static final int FLAG_Y_POS = 115;
    private static final int CASTLE_Y_POS = 145;
    private Image imageBackground1;
    private Image imageBackground2;
    private Image castle;
    private Image start;

    private int background1PosX;
    private int background2PosX;
    private int xSpeed;
    private int xPos;
    private int floorOffsetY;
    private int heightLimit;

    private Mario mario;
    private Enemy mushroom;
    private Enemy turtle;

    private Image imageFlag;
    private Image imageCastle;

    private ArrayList<GameObject> objects;
    private ArrayList<Money> monies;

    Platform() {
        super();
        this.background1PosX = -50;
        this.background2PosX = 750;
        this.xSpeed = 0;
        this.xPos = -1;
        this.floorOffsetY = 293;
        this.heightLimit = 0;
        this.imageBackground1 = Utils.getImage(Res.IMG_BACKGROUND);
        this.imageBackground2 = Utils.getImage(Res.IMG_BACKGROUND);
        this.castle = Utils.getImage(Res.IMG_CASTLE);
        this.start = Utils.getImage(Res.START_ICON);

        CharactersFactory factory=new CharactersConcreteFactory();
        mario =     factory.createMario(300, 245);
        mushroom =  factory.createAMushroom(800, 263);
        turtle =    factory.createATurtle(950, 243);

        this.imageCastle = Utils.getImage(Res.IMG_CASTLE_FINAL);
        this.imageFlag = Utils.getImage(Res.IMG_FLAG);

        objects = new ArrayList<>();

        this.objects.add(new Tunnel(600, 230 ));
        this.objects.add(new Tunnel(1000, 230));
        this.objects.add(new Tunnel(1600, 230));
        this.objects.add(new Tunnel(1900, 230));
        this.objects.add(new Tunnel(2500, 230));
        this.objects.add(new Tunnel(3000, 230));
        this.objects.add(new Tunnel(3800, 230));
        this.objects.add(new Tunnel(4500, 230));

        this.objects.add(new Block(400, 180));
        this.objects.add(new Block(1200, 180));
        this.objects.add(new Block(1270, 170));
        this.objects.add(new Block(1340, 160));
        this.objects.add(new Block(2000, 180));
        this.objects.add(new Block(2600, 160));
        this.objects.add(new Block(2650, 180));
        this.objects.add(new Block(3500, 160));
        this.objects.add(new Block(3550, 140));
        this.objects.add(new Block(4000, 170));
        this.objects.add(new Block(4200, 200));
        this.objects.add(new Block(4300, 210));

        monies = new ArrayList<>();
        this.monies.add(new Money(402, 145) );
        this.monies.add(new Money(1202, 140));
        this.monies.add(new Money(1272, 95) );
        this.monies.add(new Money(1342, 40) );
        this.monies.add(new Money(1650, 145));
        this.monies.add(new Money(2650, 145));
        this.monies.add(new Money(3000, 135));
        this.monies.add(new Money(3400, 125));
        this.monies.add(new Money(4200, 145));
        this.monies.add(new Money(4600, 40) );

        this.setFocusable(true);
        this.requestFocusInWindow();

        this.addKeyListener(new Keyboard());
    }


    public Mario getMario() {
        return mario;
    }

    public int getFloorOffsetY() {
        return floorOffsetY;
    }

    public int getHeightLimit() {
        return heightLimit;
    }

    public int getxSpeed() {
        return xSpeed;
    }

    public int getXPos() {
        return xPos;
    }

    public void setBackground2PosX(int background2PosX) {
        this.background2PosX = background2PosX;
    }

    public void setFloorOffsetY(int floorOffsetY) {
        this.floorOffsetY = floorOffsetY;
    }

    public void setHeightLimit(int heightLimit) {
        this.heightLimit = heightLimit;
    }

    public void setXPos(int xPos) {
        this.xPos = xPos;
    }

    public void setxSpeed(int xSpeed) {
        this.xSpeed = xSpeed;
    }

    public void setBackground1PosX(int x) {
        this.background1PosX = x;
    }

    public void updateBackgroundOnMovement() {
        if (this.xPos >= 0 && this.xPos <= 4600) {
            this.xPos = this.xPos + this.xSpeed;
            // Moving the screen to give the impression that Mario is walking
            this.background1PosX = this.background1PosX - this.xSpeed;
            this.background2PosX = this.background2PosX - this.xSpeed;
        }

        // Flipping between background1 and background2
        if (this.background1PosX == -800) {
            this.background1PosX = 800;
        } else if (this.background2PosX == -800) {
            this.background2PosX = 800;
        } else if (this.background1PosX == 800) {
            this.background1PosX = -800;
        } else if (this.background2PosX == 800) {
            this.background2PosX = -800;
        }
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics g2 = g;

        for (GameObject object : objects) {
            if (this.mario.isNearby(object))
                this.mario.contact(object);

            if (this.mushroom.isNearby(object))
                this.mushroom.contact(object);

            if (this.turtle.isNearby(object))
                this.turtle.contact(object);
        }

        for (int i = 0; i < monies.size(); i++) {
            if (this.mario.contactPiece(this.monies.get(i))) {
                Audio.playSound(Res.AUDIO_MONEY);
                this.monies.remove(i);
            }
        }

        if (this.mushroom.isNearby(turtle)) {
            this.mushroom.contact(turtle);
        }
        if (this.turtle.isNearby(mushroom)) {
            this.turtle.contact(mushroom);
        }
        if (this.mario.isNearby(mushroom)) {
            this.mario.contact(mushroom);
        }
        if (this.mario.isNearby(turtle)) {
            this.mario.contact(turtle);
        }

        // Moving fixed GameComponent.objects
        this.updateBackgroundOnMovement();
        if (this.xPos >= 0 && this.xPos <= 4600) {
            for (GameObject object : objects) {
                object.move();
            }

            for (Money money : monies) {
                money.move();
            }

            this.mushroom.move();
            this.turtle.move();
        }

        g2.drawImage(this.imageBackground1, this.background1PosX, 0, null);
        g2.drawImage(this.imageBackground2, this.background2PosX, 0, null);
        g2.drawImage(this.castle, 10 - this.xPos, 95, null);
        g2.drawImage(this.start, 220 - this.xPos, 234, null);

        for (GameObject object : objects) {
            g2.drawImage(object.getImageObject(), object.getX(),
                    object.getY(), null);
        }

        for (Money money : monies) {
            g2.drawImage(Utils.getImage(money.imageOnMovement()), money.getX(),
                    money.getY(), null);
        }

        g2.drawImage(this.imageFlag, FLAG_X_POS - this.xPos, FLAG_Y_POS, null);
        g2.drawImage(this.imageCastle, CASTLE_X_POS - this.xPos, CASTLE_Y_POS, null);

        if (this.mario.isJumping())
            g2.drawImage(Utils.getImage(this.mario.doJump()), this.mario.getX(), this.mario.getY(), null);
        else
            g2.drawImage(Utils.getImage(this.mario.walkImageURL(Res.IMAGE_PREFIX_CHARACTER_MARIO, MARIO_FREQUENCY)), this.mario.getX(), this.mario.getY(), null);

        if (this.mushroom.isAlive())
            g2.drawImage(Utils.getImage(this.mushroom.walkImageURL(Res.IMAGE_PREFIX_CHARACTER_MUSHROOM, MUSHROOM_FREQUENCY)), this.mushroom.getX(), this.mushroom.getY(), null);
        else
            g2.drawImage(Utils.getImage(this.mushroom.deadImageURL(Res.IMAGE_PREFIX_CHARACTER_MUSHROOM)), this.mushroom.getX(), this.mushroom.getY() + MUSHROOM_DEAD_OFFSET_Y, null);

        if (this.turtle.isAlive())
            g2.drawImage(Utils.getImage(this.turtle.walkImageURL(Res.IMAGE_PREFIX_CHARACTER_TURTLE, TURTLE_FREQUENCY)), this.turtle.getX(), this.turtle.getY(), null);
        else
            g2.drawImage(Utils.getImage(this.turtle.deadImageURL(Res.IMAGE_PREFIX_CHARACTER_TURTLE)), this.turtle.getX(), this.turtle.getY() + TURTLE_DEAD_OFFSET_Y, null);
    }

}
