package game;

import utils.Utils;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

public class Audio {
    private Clip clip;

    public Audio(String song) {

        try {
            AudioInputStream audio = AudioSystem.getAudioInputStream(getClass().getResource(song));
            clip = AudioSystem.getClip();
            clip.open(audio);
        } catch (Exception e) {
            Utils.printLog(System.err,e.getCause().toString());
        }
    }

    public Clip getClip() {
        return clip;
    }

    public void play() {
        clip.start();
    }

    public void stop() {
        clip.stop();
    }

    public static void playSound(String son) {
        Audio s = new Audio(son);
        s.play();
    }

    public void playLoop() {
        clip.loop(Clip.LOOP_CONTINUOUSLY);
    }
}
